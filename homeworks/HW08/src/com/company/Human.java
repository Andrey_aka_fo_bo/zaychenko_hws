package com.company;

public class Human {
    //поля
    private String name; // имя Human
    private int weight; // вес Human

    public String getName(){
        return name;
    } // выдаем имя из класса Human
    public void setName(String name){
        this.name = name;
    }

    public int setWeight(int weight) { // выдаем вес из класса Human
        if (weight > 0 && weight < 150){ // проверяем, чтобы вес был больше нуля
            this.weight = weight;
        }
        return weight;
    }
}
