package com.company;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        Human andrey = new Human();
        Human vasya = new Human();
        Human petya = new Human();
        Human vika = new Human();
        Human klava = new Human();
        Human luba = new Human();
        Human lilya = new Human();
        Human filip = new Human();
        Human patifon = new Human();
        Human fler = new Human();

        andrey.setWeight(86);
        vasya.setWeight(68);
        petya.setWeight(94);
        vika.setWeight(34);
        klava.setWeight(51);
        luba.setWeight(75);
        lilya.setWeight(44);
        filip.setWeight(38);
        patifon.setWeight(102);
        fler.setWeight(85);

        int[] numIn = new int[] {petya.setWeight(34),andrey.setWeight(86), klava.setWeight(51), vasya.setWeight(68),  vika.setWeight(44),
                luba.setWeight(75), lilya.setWeight(44), filip.setWeight(38),patifon.setWeight(102), fler.setWeight(85)};
        System.out.println("Исходные значения веса Human = " + Arrays.toString(numIn));

        for (int left = 0; left < numIn.length; left++) {
            int value = numIn[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < numIn[i]) {
                    numIn[i + 1] = numIn[i];
                } else {
                    break;
                }
            }
            numIn[i + 1] = value;
        }
        System.out.println("Веса Human по возрастанию  = " + Arrays.toString(numIn));
    }
}
